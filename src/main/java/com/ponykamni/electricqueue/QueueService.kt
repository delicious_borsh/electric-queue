package com.ponykamni.electricqueue

import org.springframework.stereotype.Service

@Service
class QueueService internal constructor() {

    private val queueMembers = HashMap<Int, String>()
    var currentNumber = 0

    fun appendToQueue(name: String): Int {
        queueMembers[currentNumber] = name
        println(queueMembers)
        return currentNumber++
    }

    companion object {
        private const val MAXIMUM_QUANTITY = 20
    }
}

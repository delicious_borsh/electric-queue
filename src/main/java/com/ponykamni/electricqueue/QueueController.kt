package com.ponykamni.electricqueue

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam

@Controller
class QueueController @Autowired
internal constructor(private val service: QueueService) {

    @RequestMapping(value = ["/greeting"], method = [RequestMethod.GET])
    fun greeting(
        model: Model
    ): String {
        model.addAttribute("currentNumber", service.currentNumber)
        return "greeting"
    }

    @RequestMapping(value = ["/test"], method = [RequestMethod.POST])
    fun test(
        @RequestParam(name = "name", required = false, defaultValue = "World") name: String,
        @RequestParam(name = "subject", required = false, defaultValue = "dlrow") subject: String,
        model: Model
    ) {
        model.addAttribute("name", name)
        model.addAttribute("number", service.appendToQueue(name))
    }
}
